# OpenUp Project Summary

For more information, please contact: 

Andrew Tobin, Managing Director, EMEA, Evernym. andrew.tobin@evernym.com

## Introduction

At Evernym, we have a history of open sourcing our work. Until now, we have kept the top layer of our SSI architecture proprietary. This layer provides a much easier to use SDK and platform for developers to create compelling SSI deployments compared to the lower level Hyperledger Indy/Ursa/Aries stack.

We want to encourage anyone to develop SSI solutions. For this reason, we will use our ESSIF-Lab grant to convert our proprietary software into repositories covered by a business source license, which itself will convert to an open source Apache 2.0 license after 36 months.

This will enable anyone to use and develop on our code, with all non production use permitted as well as production use up to a limit.

We believe this will accelerate SSI adoption, removing technical and commercial barriers to entry, by providing the world with access to the best SSI technology available.

Evernym's products that are the target of this open sourcing initiative cover enterprise and consumer uses:
* Verity: our enterprise SSI SDK and server to operate on-premise or as Saas. This performs all enterprise functions that are needed in an SSI ecosystem including forming DID connections, authentication, credential issuance and verification.
* Mobile SDK: our app SDK to allow anyone to build Android or iOS based apps with full SSI wallet functionality.
* Connect.Me: our mobile app that provides SSI wallet capability for making DID connections, holding credentials and sending proofs.

### About Evernym
Evernym was founded in 2013 to solve the digital identity crisis. We envisioned a world where consumers are in complete control of their digital identity, where privacy is a basic human right, and where consumers and organizations can foster a new relationship rooted in trust.

And today, we’re at the forefront of a rapidly growing global movement to decentralize digital identity.

We recognized early on that, in order to truly fix the digital identity crisis, we would need something that broke that status quo. Something that would add a much-needed layer of trust to the Internet. We knew this vision (what we now call self-sovereign identity) was bigger than any one company, and that in order for decentralized identity to take off, it had to be… well, decentralized.

That’s why, in 2016, we invented the Sovrin Network (the first and only public ledger specifically built for identity) and entrusted it to the Sovrin Foundation.

With the invention of Sovrin and the use of distributed ledger technology, we finally had the infrastructure to store and verify digital credentials in a tamper-proof, decentralized manner. This, in turn, offered a way to give consumers sole control over their online existence—moving their personal information out of company databases and into their possession.

We also realized that self-sovereign identity must be interoperable and network-agnostic to reach its full potential. We wanted to make sure that SSI’s digital credentials could be used by anyone, anywhere—no matter what identity wallet, ledger, or software they use.

That’s why we then co-invented an Internet-wide standard for the foundational component of decentralized identity (Decentralized Identifiers), in collaboration with the W3C Credentials Community Group and DIF, with funding from the U.S. Department of Homeland Security.

Lastly, we realized that the developer community would be key to driving adoption, which is why we donated the original open-source codebase that became Hyperledger Indy. And it’s why we’re also leading contributors to the Hyperledger Ursa shared crypto library now used by all Hyperledger projects and the new Hyperledger Aries project for interoperable cross-ledger SSI digital wallets and agents. 

With these three critical pieces in play—the ledger, the standards, and the community—self-sovereign identity became a reality and our true work began.

## Summary

### Business Problem
To maximise the uptake of SSI, we want to remove as many boundaries as possible. We want people to be able to experiment, create prototypes, and turbocharge innovation. For this reason, Evernym is opening up our proprietary code bases for our core products, and making them publicly available. 

These core products power credential exchange ecosystems. They include Verity, our enterprise credential exchange platform, and Connect.Me, our consumer mobile app.

Our project is a bit different to most of the other eSSIF-Lab projects, in that we are providing the layer 2 and 3 credential exchange infrastructure that many other SSI projects, including eSSIF-Lab projects, can build their business solutions on top of.

### Technical Solution

As described in the [Project Plan](https://gitlab.grnet.gr/essif-lab/infrastructure/evernym/deliverables/-/blob/master/README.md), the goal of this project is to make existing code bases publicly available.

The target code bases are:

[Evernym Verity](https://github.com/evernym/verity), including the [Verity SDK](https://github.com/evernym/verity-sdk)
* Implements an enterprise agent for issuing and verifying

[Evernym Mobile SDK](https://github.com/evernym/mobile-sdk)
* Can be used to create mobile holder agents for iOS or Android

[Evernym Connect.Me](https://github.com/evernym/connectme)
* Implements a mobile holder agent for iOS or Android

## Integration with eSSIF-Lab Functional Architecture and Community

This map conveys how the codebases that Evernym is opening up fit into the eSSIF-Lab functional architecture. Evernym's software uses the Hyperledger Aries protocols for DIDComm and credential exchange, so all the API information can be found in the [Aries specification](https://github.com/hyperledger/aries).

[Mapping To eSSIF-Lab Architecture](https://gitlab.grnet.gr/essif-lab/infrastructure/evernym/deliverables/-/blob/master/Evernym_Mapping_to_eSSIF-Lab_Functional_Architecture.pdf)